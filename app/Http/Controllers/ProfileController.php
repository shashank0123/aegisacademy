<?php

namespace App\Http\Controllers;

 use Softon\Indipay\Facades\Indipay;
use Illuminate\Http\Request;
// use Mail;
use App\Jobs\Premium;

class ProfileController extends Controller
{
    public function index()
    {
        return view('profile.index');
    }

    public function acheivements()
    {
        return view('profile.acheivements');
    }

    public function security()
    {
        return view('profile.security');
    }

    public function downgrade()
    {
        auth()->user()->is_premium = 0;
        auth()->user()->save();

        return back();
    }

    public function upgrade()
    {
        $uid = uniqid();
         $parameters = [
            'tid' => '1001' . $uid,
            'order_id' => $uid,
            'purpose' => 'Premium Subscription',
            'amount' => 1500.0,
            'buyer_name' => \Auth::user()->name,
            'email' => \Auth::user()->email,
            'phone' => '9560755823',
          ];


          $purchaseOrder = Indipay::prepare($parameters);

          return Indipay::process($purchaseOrder);

           // auth()->user()->is_premium = 1;
           // auth()->user()->save();

           // return back();
    }

    public function upgradeSuccess(Request $request)
    {
       $response = Indipay::response($request);
       if(isset($response->status) && $response->status == 'Credit')
        {
            auth()->user()->is_premium = 1;
            auth()->user()->save();

            // send premium email
            $this->sendPremiumEmail();

          return redirect('/profile');
        }
        if ($request->payment_status != 'Failed'){
            auth()->user()->is_premium = 0;
            auth()->user()->save();

          return redirect('/profile');

        } else {
          return redirect('/profile');
        }

    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required']);

        auth()->user()->update($request->all());

        // flash('Your Profile was updated successfully')->success();


        return back()->with('message', 'Your Profile was updated successfully');

    }

    public function updatePassword(Request $request)
    {
          $this->validate($request, [
              'old_password' => 'required',
              'password' => 'required|string|min:6|confirmed'
            ]);

          $old_password = $request->get('old_password');
          $password = $request->get('password');

          if(!\Hash::check($old_password, auth()->user()->password))
          {
            // flash('Please enter your current password correct!')->success();

              return back()->with('message', 'Please enter your current password correct!');
          }

          auth()->user()->password = bcrypt($password);
          auth()->user()->save();

           // flash('Your password was successfully updated!')->success();

          return back()->with('message', 'Your password was successfully updated!');
    }

    /**
    * Send an e-mail welcome to the user.
    *
    * @return Response
    */
   public function sendPremiumEmail()
   {
     $user = auth()->user();
     try {
       // Mail::send('emails.premium', ['user' => $user], function ($m) use ($user) {
       //     $m->from( env('MAIL_USERNAME'), env('APP_NAME') );
       //
       //     $m->to($user->email, $user->name)->subject('Welcome to our premium plan');
       // });

       Premium::dispatch($user)
                      ->onQueue('mail_queue')
                      ->onConnection('database');
     }
     catch(Exception $e)
     {

     }
   }
   // profile image upload
   public function fileUpload(Request $request) {
    $this->validate($request, [
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
    ]);

    if ($request->hasFile('image')) {
        $image = $request->file('image');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploadContent/profiles');
        $image->move($destinationPath, $name);
        auth()->user()->image = $name;
        auth()->user()->save();

        return back()->with('upload-message','Profile Upload successfully');
    }
}
}
