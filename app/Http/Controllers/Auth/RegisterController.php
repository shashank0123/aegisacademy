<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Jobs\SendWelcomeEmail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      $user =  User::create([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => Hash::make($data['password']),
      ]);
      // send register welcome email
      $this->sendRegisterEmail($user);
        return $user;
    }

    /**
    * Send an e-mail welcome to the user.
    *
    * @param  User  $user
    * @return Response
    */
   public function sendRegisterEmail($user)
   {
     try {
       // Mail::send('emails.register', ['user' => $user], function ($m) use ($user) {
       //     $m->from( env('MAIL_USERNAME'), env('APP_NAME') );
       //
       //     $m->to($user->email, $user->name)->subject('Welcome to '.env('APP_NAME'));
       // });
       SendWelcomeEmail::dispatch($user)
                      ->onQueue('mail_queue')
                      ->onConnection('database');
     }
     catch(Exception $e)
     {

     }
   }

}
