<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Models\Wiki;
use App\Http\Requests\WikiSectionRequest as StoreRequest;
use App\Http\Requests\WikiSectionRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class WikiSectionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class WikiSectionCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\WikiSection');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/wikisection');
        $this->crud->setEntityNameStrings('wikisection', 'wiki_sections');

        // $this->crud->setRoute(config('backpack.base.route_prefix') . '/wikisection?wiki=' . request('wiki'));
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/wikisection');

        $this->crud->backlink = config('backpack.base.route_prefix') . '/wikisection?wiki=' . request('wiki');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        // add asterisk for fields that are required in WikiSectionRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->addFields([

            ['label' => 'Title', 'name' => 'title', 'type' => 'text'],

            ['label' => 'Data', 'name' => 'data', 'type' => 'latex'],




        ]);

        if(request()->has('wiki'))
        {
            $this->crud->addField([  // Select2
               'label' => "Wiki",
               'type' => 'hidden',
               'name' => 'wiki_id',
               'value' => request('wiki')

            ]);

            $this->wiki = Wiki::findOrFail(request('wiki'));
            $this->crud->addClause('where', 'wiki_id', '=', $this->wiki->id);
            $this->crud->setEntityNameStrings(' Wiki Page Section', 'Wiki Page Section');

            $this->crud->headlink = config('backpack.base.route_prefix') . '/wikis?wiki='.$this->wiki->id;

            $this->crud->headname = 'all wiki pages';



            // $this->crud->setRoute(config('backpack.base.route_prefix') . '/problem-questions?problem=' . $this->problem->id);

        } else {
            $this->crud->addField( [  // Select2
               'label' => "Problem Set",
               'type' => 'select2',
               'name' => 'wiki_id', // the db column for the foreign key
               'entity' => 'wiki', // the method that defines the relationship in your Model
               'attribute' => 'title', // foreign key attribute that is shown to user
               'model' => "App\Models\Wiki", // foreign key model
               'allows_null' => false
            ]
            );
             $this->crud->setEntityNameStrings('Section', 'Sections');

            // $this->crud->setRoute(config('backpack.base.route_prefix') . '/problem-questions');
        }

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->removeAllButtons();
        $this->crud->addButtonFromModelFunction('line', 'editSection', 'editSection', 'beginning');
        $this->crud->addButtonFromModelFunction('line', 'deleteSection', 'deleteSection', 'end');
        $this->crud->addButtonFromModelFunction('top', 'addSection', 'addSection', 'beginning');
    }




    public function performSaveAction($itemId = null)
    {
        $saveAction = \Request::input('save_action', config('backpack.crud.default_save_action', 'save_and_back'));
        $itemId = $itemId ? $itemId : \Request::input('id');

        switch ($saveAction) {
            case 'save_and_new':
                $redirectUrl = 'admin/wikisection/create?wiki=' . $this->crud->entry->wiki->id;
                break;
            case 'save_and_edit':
                $redirectUrl = 'admin/wikisection'.'/'.$itemId.'/edit?wiki='. $this->crud->entry->wiki->id;
                if (\Request::has('locale')) {
                    $redirectUrl .= '&locale='.\Request::input('locale');
                }
                break;
            case 'save_and_back':
                $redirectUrl = 'admin/wikisection'.'/'.$itemId.'/edit?wiki='. $this->crud->entry->wiki->id;
                if (\Request::has('locale')) {
                    $redirectUrl .= '&locale='.\Request::input('locale');
                }
            default:
                $redirectUrl = 'admin/wikisection?wiki=' . $this->crud->entry->wiki->id;
                break;
        }

        return \Redirect::to($redirectUrl);
    }

    /**
        * Display all rows in the database for this entity.
        *
        * @return Response
        */
       public function index()
       {
           $this->crud->hasAccessOrFail('list');

           $this->data['crud'] = $this->crud;
           
           $this->data['title'] = ucfirst((request('wiki') ? $this->wiki->id . ' --> ' : '') . $this->crud->entity_name_plural);

           // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
           return view($this->crud->getListView(), $this->data);
       }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
