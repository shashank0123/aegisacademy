<?php

namespace App\Http\Controllers;

use App\Models\Wiki;
use App\Models\WikiSection;
use Illuminate\Http\Request;
use StdClass;
use Log;

class WikiSectionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($wikiId = null)
    {
        $wikigroup = "All Wiki Pages Section";
        if(isset($wikiId))
        {
           $articles = WikiSection::where('wiki_id', $wikiId)->orderBy('created_at', "DESC");
           $wikis = Wiki::where(['id'=> $wikiId])->first();
           $wikigroup = ' Wiki Page Section of '.$wikis->title;
        } else {
           $articles = WikiSection::orderBy('created_at', "DESC");
        }

        if(request()->has('query') && request('query') != '')
        {
            $articles = $articles->where('title', 'Like', '%' . request('query') . '%')->paginate(10);
            $wikigroup = $wikigroup . ' related to \'' . request('query') . '\'';
        } else {
            $articles = $articles->paginate(10);
        }

        return view('wikisection.index', compact('articles', 'wikiId', 'wikigroup'));
    }


    /**
     * Show single wiki page by slug
     *
     * @return wiki show page
     */

    public function show($wikiId, $wikiSectionId)
    {
        $wiki = Wiki::where(['id'=> $wikiId])->first();
        $wikisection = WikiSection::where(['wiki_id'=> $wikiId, 'id' => $wikiSectionId])->first();

        return view('wikisection.show', compact('wiki','wikisection'));
    }

    /**
    * Get body of wiki by id
    *
    * @return wiki show page
    */
    public function getBody(WikiSection $wikiSection)
    {
      // Log::info('jhi');
        return response(['body' => $wikiSection->data], 200);
    }

    /**
     * update body of wiki by id
     *
     * @return wiki show page
     */

    public function updateBody(Request $request, Wiki $wiki)
    {
        $wiki->body = $request->get('body');

        $wiki->save();

        return response([$request->get('body')], 200);
    }

    /**
     * Update the wiki page
     *
     * @return wiki show page
     */

    public function update(Request $request, Wiki $wiki)
    {
        $request->validate([
                'comment'  => 'required',
            ]);

        $wiki->body = $request->get('comment');

        $wiki->save();

        return redirect($wiki->url);
    }
}
