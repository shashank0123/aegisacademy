
<div class="table-of-content" style="    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, .1);
    padding: 0.5rem;
    background-color: #f1f5f8;
    padding-right: 2rem;
    max-width: 24rem;
    margin-top: 2rem;
    margin-bottom: 2rem;
    box-sizing: inherit;">
  <h3 class="">Contents of Wiki Section</h3>
    <ul style="list-style: none;padding-top: 5px;">
      @foreach($wikisection as $index => $section)
      <li style="list-style-type: none;" class="mb-2">
        <a name="{{$index + 1}}-{{ $section->title }}" href="#wikiSection{{$index + 1}}" onclick="myFunc(this.name);" class="scrollOnTitle" style=" cursor: pointer;     text-decoration: none;
    color: #606f7b;
    font-size: 1.125rem;">{{$index + 1}}. {{ $section->title }}</a>
      </li>
      @endforeach
    </ul>
  </div>
  <p></p>
  @foreach($wikisection as $index => $section)
  <div class="" name="#{{$index + 1}}-{{ $section->title }}" id="wikiSection{{$index + 1}}">
    <h3 >{{ $section->title }}</h3>
  </div>
  <latex-editor source-url="/wiki_section/{{ $section->id }}/body"></latex-editor>
  <br/>
  @endforeach


  <script type="text/javascript">

      function myFunc(name) {
        // var a = $(this).attr('name');
        var nav = $('#'+name);
        if (nav.length) {
          // alert("Value of variable a is : " + name );
          // var contentNav = nav.offset().top;
          // $(window).scrollTop( $("#"+name).offset().top );
          $('html, body').animate({ scrollTop: $('#wikiSection2').offset().top }, 'slow');
            return false;

          $('html, body').animate({
           scrollTop: $("#"+name).offset().top
         }, 2000);
        }


            }

            $(".scrollOnTitle").click(function() {
              var a = $(this).attr('name');
    $('html, body').animate({
        scrollTop: $("#"+a).offset().top
    }, 2000);
});
	</script>
