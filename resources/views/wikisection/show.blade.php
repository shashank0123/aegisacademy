@extends('layouts.app')


@section('content')

	<div class="bg-grey-lighter h-auto" style="min-height: 100vh;">
		<div class="bg-white shadow-md container p-3 px-6 pt-4 md:mx-auto">

            <span class=" ttlresp text-blue-dark text-sm font-semibold tracking-wide block mb-2 ">
							<a style="font-weight: bold;" href="/wiki/category:{{$wiki->category->id}}" class="no-underline text-blue-dark hover:text-brand">
							 {{ $wiki->category->name }}
							</a>
              <i class="fa fa-chevron-right text-grey-dark"></i>
							<a style="font-weight: bold;" href="/wiki/{{$wiki->slug}}" class="no-underline text-blue-dark hover:text-brand">
							 {{ $wiki->title }}
							</a>
              <i class="fa fa-chevron-right text-grey-dark"></i>
							<span class="text-grey-darker">{{ $wikisection->title }}</span>
            </span>

			<h2 style="font-weight: bold!important; padding: 15px 10px;" class="text-black mb-35 font-semibold text-3xl pt-4 border-t-0 border-x-0 border-b border-black pb-3">  {{ $wikisection->title }}</h2>

			<latex-editor source-url="/wiki_section/{{ $wikisection->id }}/body"></latex-editor>

		</div>




	</div>

@endsection
